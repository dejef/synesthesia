'use strict';

// Framework
var express = require('express');
var app = express();

// business logic
var service = require("./service.js");

// Uploading a file requires this middleware
var multer  = require('multer')
const path = require('path');

const request = require('request');
const os = require('os');

var SpotifyWebApi = require('spotify-web-api-node');
console.log(os.tmpdir());
// template engine for changing uri
app.set("view engine","ejs");
app.set('views', path.join(__dirname, '/public'));

var image_label;

var client_id = '07c40b4d1267440199f95803058207e6';
var client_secret = '6444f551d60947998687d3646e4d1dcc';

// Authorize -> get a category's playlist id -> retrieve playlist
var authOptions = {
    url: 'https://accounts.spotify.com/api/token',
    headers: {
      'Authorization': 'Basic ' + (new Buffer(client_id + ':' + client_secret).toString('base64'))
    },
    form: {
      grant_type: 'client_credentials'
    },
    json: true
  };
  

// Spotify API credentials are optional (no need for ours now)
var spotifyApi = new SpotifyWebApi({
    clientId: client_id,
    clientSecret: client_secret,
    redirectUri: 'http://www.duckduckgo.com/'   // unused field
  });

const multer_storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, os.tmpdir())
    },
    filename: function (req, file, cb) {
      cb(null, file.originalname)
    }
  })

const upload = multer({ 
    fileFilter: function (req, file, cb) {
        var filetypes = /jpeg|jpg|JPEG|JPG|png|PNG/;
        var mimetype = filetypes.test(file.mimetype);
        var extname = filetypes.test(path.extname(file.originalname).toLowerCase());

        if (mimetype && extname) {
            return cb(null, true);
        }
        cb("Error: File upload only supports the following filetypes - " + filetypes);
    },
    storage: multer_storage
});

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/public/index.html');
});

app.get('/banner.png', function(req, res) {             // could be served by gcloud ?
    res.sendFile(__dirname + '/public/banner.png');
});

app.post('/uploads', upload.single('picture'), function (req, res, next) {
    res.status(200);
    // res.set({'Content-Type': 'text/plain'});
    if (req.file == null) {
        return res.redirect('/');
    }
    service.get_tag_data(req.file.filename)
    .then(function(data) {
        image_label = data;
    },
    function(err) {
        console.log(err);
    }).then(function () {
        return res.redirect('/player');
    });
});

// https://developer.spotify.com/dashboard/ for register the application
app.get('/player', function(req, res) {
    var uri;
    request.post(authOptions, function(error, response, body) {
      if (!error && response.statusCode === 200) {
        var token = body.access_token;  // var refresh_token = body.refresh_token;
        
        spotifyApi.setAccessToken(token);

        spotifyApi.searchTracks('artist:' + image_label)
        .then(function(data) {
            if (data.body.tracks.items !== undefined && data.body.tracks.items.length !== 0 && data.body.tracks.items[0] !== undefined && data.body.tracks.items[0].uri !== undefined) {
                uri = data.body.tracks.items[0].uri;
                uri = uri.split(":")[2];
                console.log(uri)
                return res.render('player', {uri: uri});
            }
            return res.render('player', {uri: '0kesmP5jKuavoUDwuoMG3l'})
        }, function(err) {
            console.log('Something went wrong!', err);
            return res.redirect('/');
        });
      }
      else {
        console.log(error)
        return res.redirect('/'); // currently no good soln for error
      }
    });
});

app.listen(process.env.PORT || 8080);

// Resources
// https://stackoverflow.com/questions/15772394/how-to-upload-display-and-save-images-using-node-js-and-express
// https://shiya.io/simple-file-upload-with-express-js-and-formidable-in-node-js/
// https://cloud.google.com/docs/authentication/production TODO
// https://github.com/expressjs/multer/issues/114
// https://github.com/expressjs/multer/issues/439
// https://developers.google.com/web/fundamentals/primers/promises
// https://developer.spotify.com/