const vision = require('@google-cloud/vision');
const os = require('os');

module.exports.get_tag_data = get_tag_data;

// Google Vision
// this function should probably take arguments (what to be searched for label detection)
// new idea, take all of the uploads and put them in a bucket on google cloud, then use that
// bucket as the source of image detection
async function get_tag_data(filename) {
    // Creates a client
    const client = new vision.ImageAnnotatorClient();

    // Performs label detection on the image file
    const [result] = await client.labelDetection(os.tmpdir() + '/' + filename);
    const labels = result.labelAnnotations;
    labels.forEach(label => console.log(label.description));       // debug
    console.log(labels)
    let i = Math.floor(Math.random() * 5);
    if (labels !== undefined && labels.length !== 0 && labels[i] !== undefined && labels[i].description !== undefined) {
        return labels[i].description;
    }
    return null;
}